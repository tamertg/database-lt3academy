create database lt3Academy 

	/* Notes
	
	(A) Stored procedures:

		1-Stored procedure (spNewPreApprentice) for new preap with adding Notes 
			and with new cohort or without adding cohort.
			BY
			(exec spNewPreApprentice 'cohortName', 'lastName', 'firstName', 'gender'(F OR M), 'county' ('CSTP'OR'CSTB'), 
			'email','phone','techQuestCompleted'('Y' OR 'N'),'computerAccess'('Y' OR 'N'),
			'classification'('Adult'OR 'Youth'),'Status','notes','cohortName','cohortStartDate'
			OR
			(exec spNewPreApprentice 'cohortName', 'lastName', 'firstName', 'gender'(F OR M), 'county' ('CSTP'OR'CSTB'), 
			'email','phone','techQuestCompleted'('Y' OR 'N'),'computerAccess'('Y' OR 'N'),
			'classification'('Adult'OR 'Youth'),'Status','notes'
		2-Stored procedure (spAddPreApprenticeWithoutNotes) for adding new preApprentice to Existed Cohort 
			wothout Notes (Notes by default ('Officially Accepted / CSTB Approved'))
			BY
			( exec spAddPreApprenticeWithoutNotes 'cohortName', 'lastName', 'firstName', 'gender'(F OR M), 
			'county' ('CSTP'OR'CSTB'), 'email','phone','techQuestCompleted'('Y' OR 'N'),
			'computerAccess'('Y' OR 'N'),'classification'('Adult'OR 'Youth'),'Status')

		3-Stored procedure (spAddActivities) for adding activities  
			BY
			(exec spAddActivities activityID,'activity','activityType').

		4- stored procedure (spNewCohort) to create a new cohort
			BY
			(exec spNewCohort 'cohortName', 'cohortStartDate')


	(B) Triggers

		1-trigger (trCohortEndDate) for the program end date (start date + 6 months).
		2. triggers (trInsertIntoActivityDates) for activity dates.

	(c) Reports 
		1-For a person / preapprentice , show me all their activity dates 
		BY
		 execute stored procedure (exec spPreApprenticeActivity 'lastName','firstName')

		2- For a cohort show me all prepapprentices
		BY
		 execute stored procedure (exec spCohortPrepapprentices 'CohortName')

		3- How many people in each cohort 
		BY
		 execute stored procedure (exec spCohortCount)

		4- For a particiluar Cohort. show me all activity dates
		BY
		 execute stored procedure (exec spCohortActivity 'CohortName')

	*/


create table cohort
(
	cohortName nvarchar (15) constraint pkCohortCohortName primary key,
	cohortStartDate date ,
	cohortEndDate date ,
	program nvarchar (50) constraint dfCohortProgramByDefault default('Pre-Apprentice Software Developer')
);

drop table preApprentice

create table preApprentice 
(
	cohortName nvarchar (15) 
	constraint fkPreApprenticeCohortNameToCohortCohortName foreign key references cohort(cohortName) ,
	lastName nvarchar (15),
	firstName nvarchar (15),
	gender nvarchar (1) constraint ckPreApprenticeGender check (gender in('F','M')) ,
	county nvarchar (4) constraint ckPreApprenticeCounty check (county in('CSTP','CSTB')),
	email nvarchar (50),
	phone nvarchar (20),
	techQuestCompleted nvarchar (1) constraint ckPreApprenticeTechQuestCompleted check (techQuestCompleted in('Y','N')),
	computerAccess nvarchar (1) constraint ckPreApprenticeComputerAccess check (ComputerAccess in('Y','N')),
	classification nvarchar (5) constraint ckClassification check (classification in ('Adult','Youth')),
	Status nvarchar (15),
	notes nvarchar (250)constraint dfPreApprenticeNotes default ('Officially Accepted / CSTB Approved'),
	constraint pkFirstNameEmail primary key (lastName, firstName, email)
);

create table activities
(
	activityID tinyint constraint pkActivitiesActivityID primary key not null,
	activity nvarchar (50),
	activityType nvarchar (50)
);


create table activityDates
(
	activityID tinyint 
	constraint fkActivityDatesActivityIDToActivitiesActivityID foreign key references activities(activityID),
	activityName nvarchar (50),
	activityDate date,
	cohortName nvarchar (15) 
	constraint fkActivityDatesActivityIDToCohortCohortName foreign key references cohort(cohortName),
	constraint pkactivityDates primary key (activityID, activityDate, cohortName)
);


select * from activities
select * from activityDates;
select * from preApprentice ;
select * from cohort;


--1. Create a stored procedure to create a new cohort [startdate, endate, program]

 

  create proc spNewCohort

	@cohortName nvarchar (15),
	@cohortStartDate date 
	as
	begin
		insert into cohort ( cohortName, cohortStartDate )
		values (@cohortName, @cohortStartDate) 
	end


	-- insert into cohort table
	exec spNewCohort 'softDev05','06/15/2022'


--2. Create a stored procedure to create a new preapprentice

	/* Stored procedure (spNewPreApprentice) for new PreApprentice with new cohort with adding Notes */
	
	create proc spNewPreApprentice

		@cohortName nvarchar (15),
		@lastName nvarchar (15),
		@firstName nvarchar (15),
		@gender nvarchar (1),
		@county nvarchar (4),
		@email nvarchar (50),
		@phone nvarchar (20),
		@techQuestCompleted nvarchar (1),
		@computerAccess nvarchar (1),
		@classification nvarchar (5),
		@Status nvarchar (15),
		@notes nvarchar (250),
		@cohortNames nvarchar (15) = null,
		@cohortStartDate date = null
		as
		
				begin
					if @cohortNames is not null and @cohortStartDate is not null
					
						insert into cohort ( cohortName, cohortStartDate)
						values ( @cohortNames, @cohortStartDate)
					
				end

			begin 

				insert into preApprentice 
							(cohortName, lastName, firstName, gender, county, email, phone, techQuestCompleted, 
							computerAccess, classification, Status,notes)
					values (@cohortName, @lastName, @firstName, @gender, @county, @email, @phone, @techQuestCompleted, 
							@computerAccess, @classification, @Status,@notes)


			end		
		


	/* Stored procedure (spAddPreApprenticeWithOrNotNotes) for adding new preApprentice to Existed Cohort wothout Notes */


	
	create proc spAddPreApprenticeWithoutNotes
	
		@cohortName nvarchar (15),
		@lastName nvarchar (15),
		@firstName nvarchar (15),
		@gender nvarchar (1),
		@county nvarchar (4),
		@email nvarchar (50),
		@phone nvarchar (20),
		@techQuestCompleted nvarchar (1),
		@computerAccess nvarchar (1),
		@classification nvarchar (5),
		@Status nvarchar (15),
		@notes nvarchar (250) = null
		as
			begin 

				insert into preApprentice 
							(cohortName, lastName, firstName, gender, county, email, phone, techQuestCompleted, 
							computerAccess, classification, Status)
					values (@cohortName, @lastName, @firstName, @gender, @county, @email, @phone, @techQuestCompleted, 
							@computerAccess, @classification, @Status)
				
			end

--3. Stored procedure (spNewActivities) for adding activities

	 
	create proc spAddActivities
		
		@activityID tinyint,
		@activity nvarchar (50),
		@activityType nvarchar (50)
	
		as
		begin 
			insert into activities (activityID, activity,activityType) values (@activityID, @activity,@activityType) 
		end

	-- insert into activity

		exec spAddActivities 1,'Review','PreApprentice Review';
		exec spAddActivities 2,'ICT Programming Essentials','Certification Exam';
		exec spAddActivities 3,'ICT Database Essentials','Certification Exam';
		exec spAddActivities 4,'CIW Database Specialist','Certification Exam';
		exec spAddActivities 5,'Agile Scrum Master','Certification Exam';
		exec spAddActivities 6,'Completion Cert','Certificate';
		exec spAddActivities 7,'PreApprenticeship Cert','Certificate';


--4- Master stored procedure for a new cohort [having someing call a SP that inputs in all necessary tables 
	--to initialize a new cohort]


--5. Insert a prepapprentice into the preappren table


	--ex for new preap with adding Notes and with new cohort or without adding cohort

		exec spNewPreApprentice 'softDev01','Mik','Jacson','M','CSTP','mik@hotmail.com','727-258-8521','Y','N',
								'Youth','CURRENT','Officially Accepted','softDev01','01/01/2022';
--EXEC new cohort one by one

		exec spNewPreApprentice 'softDev03','jordan','Coe','M','CSTB','Jordan@hotmail.com','727-612-6325','y','y',
								'Adult','CURRENT','Officially Accepted','softDev03','03/25/2022';	
--EXEC new cohort one by one

		exec spNewPreApprentice 'softDev05','Khalil','Tamer','M','CSTP','tamer@hotmail.com','727-906-9066','y','y',
								'Adult','CURRENT','Officially Accepted','softDev05','06/15/2022'
--EXEC new cohort one by one

		exec spNewPreApprentice 'softDev07','mimi','Morcos','F','CSTB','mimi@hotmail.com','727-517-7514','y','y',
								'Youth','CURRENT','Officially Accepted','softDev07','09/01/2022';
	
	--ex for new preap with notes and without adding cohort

		exec spNewPreApprentice 'softDev05','sue','yan','M','CSTb','sue@hotmail.com','727-150-987','y','y',
								'Adult','CURRENT','Officially Accepted';
	
	--ex for Adding new preap in existed cohort without Notes

		exec spAddPreApprenticeWithoutNotes 'softDev05','Donelle','Tamer','F','CSTP','Donelle@hotmail.com',
											'727-906-906', 'y','y','Adult','CURRENT'
		exec spAddPreApprenticeWithoutNotes 'softDev05','solomon','Browne','M','CSTb','solomon@hotmail.com',
											'727-578-7857','y','y','Adult','CURRENT';
		exec spAddPreApprenticeWithoutNotes 'softDev03','James','Thomas','M','CSTP','James@hotmail.com',
											'727-123-4789','y','y','Adult','CURRENT';
		exec spAddPreApprenticeWithoutNotes 'softDev03','Xan','Jo','M','CSTb','xan@hotmail.com',
											'727-354-9876','y','y','Youth','CURRENT';
		exec spAddPreApprenticeWithoutNotes 'softDev01','Morgan','jams','M','CSTP','Morgan@hotmail.com',
											'727-254-4521','Y','N','Youth','CURRENT';
		exec spAddPreApprenticeWithoutNotes 'softDev01','candy','Moe','F','CSTB','Candy@hotmail.com',
											'727-986-9856','y','y','Adult','CURRENT';
		exec spAddPreApprenticeWithoutNotes 'softDev07','Saly','Khan','F','CSTb','Saly@hotmail.com',
											'727-512-7800','y','y','Adult','CURRENT';
		exec spAddPreApprenticeWithoutNotes 'softDev07','Doglas','Stive','M','CSTb','Doglas@hotmail.com',
											'727-578-7857','y','N','Youth','CURRENT';

	--------------------------------------------------------------------------------------------
	

	delete activityDates;
	delete activities;
	delete cohort;
	delete preApprentice ;
	

	select * from activities
	select * from activityDates;
	select * from preApprentice ;
	select * from cohort;



	
--Triggers
  
-- 1. trigger for the program end date (start date + 6 months)

  create trigger trCohortEndDate on cohort
	for insert
		as
			begin 
				declare @cohortStartDate date
				select @cohortStartDate = cohortStartDate from cohort
				update cohort
				set cohortEndDate = DATEADD(MONTH,6, @cohortStartDate)
				WHERE cohortStartDate=@cohortStartDate
			end
		


-- 2. triggers for activity dates

create trigger trInsertIntoActivityDates on cohort
		after insert
		as
		begin 
		declare @cohortName nvarchar (15), @cohortStartDate date 
		set @cohortStartDate = (select cohortStartDate from inserted)
		set @cohortName = ( select cohortName from inserted  )
		insert into activityDates (activityID, activityName, activityDate, cohortName ) 
			values ( 1, 'PreApprentice Review 1', DATEADD(day,30, @cohortStartDate) , @cohortName ),
					( 1, 'PreApprentice Review 2', DATEADD(day,60, @cohortStartDate) , @cohortName ),
					( 1, 'PreApprentice Review 3', DATEADD(day,90, @cohortStartDate) , @cohortName ),
					( 1, 'PreApprentice Review 4', DATEADD(day,120, @cohortStartDate) , @cohortName ),
					( 1, 'PreApprentice Review 5', DATEADD(day,150, @cohortStartDate) , @cohortName ),
					( 1, 'PreApprentice Review 6', DATEADD(day,180, @cohortStartDate) , @cohortName ),
					( 2, 'Certification Exam', DATEADD(WEEK,6, @cohortStartDate) , @cohortName ),
					( 3, 'Certification Exam', DATEADD(WEEK,6, @cohortStartDate) , @cohortName ),
					( 4, 'Certification Exam', DATEADD(WEEK,18, @cohortStartDate) , @cohortName ),
					( 5, 'Certification Exam', DATEADD(WEEK,18, @cohortStartDate) , @cohortName ),
					( 6, 'Certificate', DATEADD(MONTH,6, @cohortStartDate) , @cohortName ),
					( 7, 'Certificate', DATEADD(MONTH,6, @cohortStartDate) , @cohortName )
		end



--Reports [Joins / Sub queuries]

	1-For a person / preapprentice , show me all their activity dates

		create proc spPreApprenticeActivity

			@lastName nvarchar (15),
			@firstName nvarchar (15)

			as 
				begin 
					select cohort.cohortName, preApprentice.lastName, preApprentice.firstName,
							activities.activity,activityName
						from activityDates
						inner join cohort on cohort.cohortName = activityDates.cohortName 
						inner join activities on activities.activityID = activityDates.activityID
						inner join preApprentice on preApprentice.cohortName = cohort.cohortName
						where lastName = @lastName and firstName = @firstName
				End

select * from activities
select * from activityDates

	exec spPreApprenticeActivity 'khalil','tamer'

	2-For a cohort show me all prepapprentices

		create proc spCohortPrepapprentices
				@cohortName nvarchar (15)
				as
					begin
						select cohort.cohortName, lastName , firstName from preApprentice
						inner join cohort on cohort.cohortName = preApprentice.cohortName
						where preApprentice.cohortName =  @cohortName
					End
						

	exec spCohortPrepapprentices 'softDev07'


	3-For a particiluar cohort. show me all their activity dates [*WHERE]
		
		create proc spCohortActivity

			@cohortName nvarchar (15)

			as 
				begin 
					select * from activityDates
						where cohortName = @cohortName
				End

	exec spCohortActivity 'softdev05'

	
	4-How many people in each cohort [*GROUP NBY with a count]

	create proc spCohortCount
			as
				begin
					select cohortName, count(cohortName) totalPreapprentice from preApprentice
					group by cohortName
				End
		
	exec spCohortCount 
	
